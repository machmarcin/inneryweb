﻿using System.Collections.Generic;
using InnergyWeb.Model;

namespace InnergyWeb
{
    public interface IItemsInStackMapper
    {
        /// <summary>
        /// GetItemsInStacks Interface
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        IEnumerable<ItemsInStack> GetItemsInStocks(string input);
    }
}
