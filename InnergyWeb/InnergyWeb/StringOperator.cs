﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace InnergyWeb
{
    public class StringOperator:IStringOperator
    {
        /// <summary>
        /// Split the text line
        /// </summary>
        /// <param name="line"></param>
        /// <param name="splitChar"></param>
        /// <returns></returns>
        public List<string> Split(string line, string splitChar = null)
        {
            List<string> result = new List<string>();            
            if(splitChar == null)
            {
                var list = line.Split(Environment.NewLine.ToArray(), StringSplitOptions.None).ToList();
                foreach(var item in list.Where(position => !String.IsNullOrWhiteSpace(position)).Distinct().ToList())
                {
                    if (!item.Contains('#'))
                        result.Add(item);
                }
                    
            }
            else
            {
                if (!line.Contains('#'))
                    result = line.Split(splitChar.ToArray()).ToList();
            }
            
            return result;
        }
    }
}
