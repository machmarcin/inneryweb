﻿using System.Collections.Generic;

namespace InnergyWeb
{
    public interface IStringOperator
    {
        /// <summary>
        /// Split interface
        /// </summary>
        /// <param name="line"></param>
        /// <param name="splitChar"></param>
        /// <returns></returns>
        List<string> Split(string line, string splitChar = null);
    }
}
