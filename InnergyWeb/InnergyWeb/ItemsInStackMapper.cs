﻿using System.Collections.Generic;
using InnergyWeb.Model;

namespace InnergyWeb
{
    public class ItemInStockMapper: IItemsInStackMapper
    {
        private StringOperator _operator;

        /// <summary>
        /// ItemInStockMapper
        /// </summary>
        /// <param name="operator"></param>
        public ItemInStockMapper(StringOperator @operator)
        {
            _operator = @operator;
        }
        /// <summary>
        /// GetItemInStock split text to stock and count of them
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public IEnumerable<ItemsInStack> GetItemsInStocks(string input)
        {
            IList<ItemsInStack> result = new List<ItemsInStack>();
            foreach (var pair in _operator.Split(input, "|"))
            {
                var items = _operator.Split(pair, ",");
                result.Add(new ItemsInStack(items[0], int.Parse(items[1])));
            }

            return result;
        }
    }


}
