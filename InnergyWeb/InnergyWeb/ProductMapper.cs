﻿using System;
using System.Collections.Generic;
using System.Linq;
using InnergyWeb.Model;

namespace InnergyWeb
{
    public class ProductMapper
    {
        private readonly IStringOperator _stringOperator;
        private readonly IItemsInStackMapper _itemsInStackMapper;

        /// <summary>
        /// ProductMapper exception control
        /// </summary>
        /// <param name="itemsInStackMapper"></param>
        /// <param name="operator"></param>
        public ProductMapper(IItemsInStackMapper itemsInStackMapper, IStringOperator @operator)
        {
            if (itemsInStackMapper == null)
                throw new ArgumentNullException(nameof(itemsInStackMapper));
            if (@operator == null)
                throw new ArgumentNullException(nameof(@operator));

            _itemsInStackMapper = itemsInStackMapper;
            _stringOperator = @operator;
        }
        /// <summary>
        /// Map line to product class
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public List<Product> MapToProducts(string input)
        {
            var productDetails = _stringOperator.Split(input, ";");
            if (productDetails.Count() > 0)
            {
                var name = productDetails[0];
                var id = productDetails[1];
                var details = productDetails[2];
                
                var items =_itemsInStackMapper.GetItemsInStocks(productDetails[2]);

                return Product.BuildForEachDetail(id, name, items);
            }

            return null;
        }
    }


}
