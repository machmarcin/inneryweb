﻿using System;
using System.Collections.Generic;
using System.Linq;
using InnergyWeb.Model;

namespace InnergyWeb
{
    public class StockReportProvider
    {
        public StockReportProvider()
        {
        }
        /// <summary>
        /// Mapping List<Product> to collection of Stackhouse
        /// </summary>
        /// <param name="productCollection"></param>
        /// <returns></returns>
        public List<Stockhouse> GetStockReport(List<Product> productCollection)
        {
            List<Stockhouse> stockhouses = new List<Stockhouse>();
            foreach(var item in productCollection.GroupBy(stock => stock.Stockhouse))
            {
                stockhouses.Add(new Stockhouse(item.Key, item.OrderByDescending(id =>id.Id).ToList()));
            }
            return stockhouses;
        }
    }
}