﻿namespace InnergyWeb.Model
{
    public class ItemsInStack
    {
        public int Amount { get; }
        public string Stock { get; }

        public ItemsInStack(string stock, int amout)
        {
            Stock = stock;
            Amount = amout;
        }
    }
}
