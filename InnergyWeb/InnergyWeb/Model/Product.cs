﻿using System.Collections.Generic;

namespace InnergyWeb.Model
{
    public class Product
    {
        public string Name { get; }
        public string Id { get; }
        public string Stockhouse { get; }
        public int Count { get; }
        public List<ItemsInStack> ItemsInStackCollection { get; }

        private Product(string id, string name, string stockhouse, int count) : this(id, name)
        {
            Id = id;
            Name = name;
            Stockhouse = stockhouse;
            Count = count;
        }

        public static List<Product> BuildForEachDetail(string id, string name, IEnumerable<ItemsInStack> items)
        {
            List<Product> result = new List<Product>();
            foreach (var item in items)
            {

                result.Add(new Product(id, name, item.Stock, item.Amount));
            }

            return result;
        }

        public Product(string id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
