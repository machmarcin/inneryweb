﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InnergyWeb.Model
{
    public class Stockhouse
    {
        public string Name { get; }
        public int Count { get; }
        public List<Product> ProductCollection { get; }

        public override string ToString()
        {
            var stock = string.Format("{0} (total {1})", Name, Count);
            StringBuilder result = new StringBuilder();
            result.Append(stock);
            foreach(var item in ProductCollection)
            {
                result.Append("\n");
                var product = string.Format("{0}: {1},", item.Id, item.Count);
                result.Append(product);
            }
            result.Append("\n");
            return result.ToString();
        }
        public Stockhouse(string name, List<Product> productCollection)
        {
            Name = name;
            ProductCollection = new List<Product>();
            foreach(var product in productCollection)
            {
                ProductCollection.Add(product);
                Count +=product.Count;
            }
    }

    }
}
