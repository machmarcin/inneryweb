﻿using InnergyWeb.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;

namespace InnergyWeb
{
    partial class Program
    {
        public static void Main(string[] args)
        {
            var text = TextTest();
            var @operator = new StringOperator();
            var itemsMapper = new ItemInStockMapper(@operator);
            var productMapper = new ProductMapper(itemsMapper, @operator);
            List<string> input = new List<string>();
            var list = @operator.Split(text);
            List<Product> productCollection = new List<Product>();
            foreach (var line in list)
            {
                productCollection.AddRange(productMapper.MapToProducts(line));
            }
            var groupedProductCollection = productCollection.GroupBy(stock => stock.Stockhouse).ToList();
            var provider = new StockReportProvider();
            var result = provider.GetStockReport(productCollection);
            StringBuilder returnText = new StringBuilder();
            foreach (var item in result)
            {
               returnText.Append(item.ToString());
                returnText.Append("\n");
            }
            Console.WriteLine(returnText);
        }
        

        public static string TextTest()
        {
            string testText = "# Material inventory initial state as of Jan 01 2018 " + Environment.NewLine +
                              "# New materials" + Environment.NewLine +
                              " Cherry Hardwood Arched Door -PS; COM - 100001; WH - A,5 | WH - B,10 " + Environment.NewLine +
                              "Maple Dovetail Drawerbox; COM - 124047; WH - A,15 " + Environment.NewLine +
                              "Generic Wire Pull; COM - 123906c; WH - A,10 | WH - B,6 | WH - C,2 " + Environment.NewLine +
                              "Yankee Hardware 110 Deg.Hinge; COM - 123908; WH - A,10 | WH - B,11 " + Environment.NewLine +
                              "# Existing materials, restocked " + Environment.NewLine +
                              "Hdw Accuride CB0115 - CASSRC - Locking Handle Kit -Black; CB0115 - CASSRC; WH - C,10 | WH - B,5 | WH - C,3 " + Environment.NewLine +
                              "Veneer - Charter Industries - 3M Adhesive Backed -Cherry 10mm - Paper Back; 3M -Cherry - 10mm; WH - A,10 | WH - B,1 " + Environment.NewLine +
                              "Veneer - Cherry Rotary 1 FSC; COM - 123823; WH - C,10" + Environment.NewLine +
                              "MDF, CARB2, 1 1 / 8 ;COM-101734; WH - C,8 ";

            return testText;
        }
    }


}
