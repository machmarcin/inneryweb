﻿using FluentAssertions;
using InnergyWeb;
using InnergyWeb.Model;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InnergyWeb.Test.Unit
{
    [TestFixture]
    public class ProductMapperTests
    {
        [Test]
        public void mapper_should_throw_exception_if_stringOperator_is_null()
        {
            var stockMapper = Mock.Of<IItemsInStackMapper>();
            Assert.Throws<ArgumentNullException>(() => new ProductMapper(stockMapper, null));
        }

        [Test]
        public void mapper_should_throw_exeption_when_stockMapper_isn_null()
        {
            var stringOperator = Mock.Of<IStringOperator>();
            Assert.Throws<ArgumentNullException>(() => new ProductMapper(null, stringOperator));
        }

        [Test]
        public void mapper_should_return_correct_values()
        {
            var stockItem = new ItemsInStack("test", 10);
            var stockMapper = Mock.Of<IItemsInStackMapper>(mapper => mapper.GetItemsInStocks(It.IsAny<string>()) == new List<ItemsInStack> { stockItem });
            var productMapper = new ProductMapper(stockMapper, new StringOperator());
            var result = productMapper.MapToProducts("testProduct;ID123;Nic");

            result.Single().Id.Should().Be("ID123");
            result.Single().Name.Should().Be("testProduct");

        }

        [Test]
        public void is_string_split_correctly()
        {
            var itemInStockMapper = new ItemInStockMapper(new StringOperator());
            var itemInStockCollection = itemInStockMapper.GetItemsInStocks("TestStockA,10|TestStockB,20|TestStockC,30").ToList();
            itemInStockCollection.Should().HaveCount(3);
            itemInStockCollection[0].Stock.Should().Be("TestStockA");
            itemInStockCollection[0].Amount.Should().Be(10);

            itemInStockCollection[1].Stock.Should().Be("TestStockB");
            itemInStockCollection[1].Amount.Should().Be(20);

            itemInStockCollection[2].Stock.Should().Be("TestStockC");
            itemInStockCollection[2].Amount.Should().Be(30);



        }
    }

}
