﻿using NUnit.Framework;
using System.Collections.Generic;
using InnergyWeb;
using InnergyWeb.Model;

namespace InnergyWeb.Test.Unit
{
    [TestFixture]
    public class StockReportsProviderTests
    {
        [Test]
        public void check_the_provider()
        {
            var products = new List<Product>
            {
                new Product("ID123", "TestProductA"),
                new Product("ID122", "TestProductB"),
                new Product("ID111", "TestProductC"),
            };

            var provider = new StockReportProvider();
            var result = provider.GetStockReport(products);
        }
    }
}
